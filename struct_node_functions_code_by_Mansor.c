#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

void printList(struct Node *head)
{
    struct Node *current = head;
    while (current != NULL)
    {
        printf("%d -> ", current->number);
        current = current->next;
    }
    printf("NULL\n");
}

void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    if (*head == NULL)
    {
        *head = newNode;
    }
    else
    {
        struct Node *current = *head;
        while (current->next != NULL)
        {
            current = current->next;
        }
        current->next = newNode;
    }
}

void prepend(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

void deleteByKey(struct Node **head, int key)
{
    struct Node *temp = *head, *prev = NULL;

    // If the key is present at the head
    if (temp != NULL && temp->number == key)
    {
        *head = temp->next;
        free(temp);
        return;
    }

    // Find the key to be deleted
    while (temp != NULL && temp->number != key)
    {
        prev = temp;
        temp = temp->next;
    }

    // If the key is not present
    if (temp == NULL)
    {
        printf("Key not found in the list.\n");
        return;
    }

    // Unlink the node from the list
    prev->next = temp->next;
    free(temp);
}

void deleteByValue(struct Node **head, int value)
{
    struct Node *temp = *head, *prev = NULL;

    // Find the node with the specified value
    while (temp != NULL && temp->number != value)
    {
        prev = temp;
        temp = temp->next;
    }

    // If the value is not present
    if (temp == NULL)
    {
        printf("Value not found in the list.\n");
        return;
    }

    // Unlink the node from the list
    prev->next = temp->next;
    free(temp);
}

void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *temp = *head;

    while (temp != NULL && temp->number != key)
    {
        temp = temp->next;
    }

    if (temp == NULL)
    {
        printf("Key not found in the list. Cannot insert.\n");
        return;
    }

    struct Node *newNode = createNode(value);
    newNode->next = temp->next;
    temp->next = newNode;
}

void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *temp = *head;

    while (temp != NULL && temp->number != searchValue)
    {
        temp = temp->next;
    }

    if (temp == NULL)
    {
        printf("Value not found in the list. Cannot insert.\n");
        return;
    }

    struct Node *newNode = createNode(newValue);
    newNode->next = temp->next;
    temp->next = newNode;
}

int main()
{
    struct Node *head = NULL;
    int choice, data, key;

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete\n");
        printf("5. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        if (choice == 1)
        {
            printList(head);
        }
        else if (choice == 2)
        {
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
        }
        else if (choice == 3)
        {
            printf("Enter data to prepend: ");
            scanf("%d", &data);
            prepend(&head, data);
        }
        else if(choice == 4)
        {
            printf("Enter key to delete: ");
            scanf("%d", &key);
            deleteByKey(&head, key);
        }
        else if (choice == 5)
        {
           exit (0);
        }
        else
        {
            printf("Invalid choice. Please try again.\n");
        }
    }
    return 0;
}
// Questions:
// Implement the prototypes defined above.
// Reimplement the main method using a switch and complete the pending steps.